#!/usr/bin/perl -w
# to use SpeedyCGI, change first line to: #!/usr/bin/speedy

use HTML::Mason::CGIHandler;
use CGI;
use CGI::Cookie;

my $h = HTML::Mason::CGIHandler->new(data_dir => "/var/cache/mason");
$h->handle_request;
