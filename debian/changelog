libhtml-mason-perl (1:1.60-2) unstable; urgency=medium

  * Remove generated test data via debian/clean. (Closes: #1045741)
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Tue, 05 Mar 2024 18:36:27 +0100

libhtml-mason-perl (1:1.60-1) unstable; urgency=medium

  * Import upstream version 1.60.
    "Fixed a test failure with Perl blead (5.37.x)."
    Closes: #1040491
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Remove test dependency on libtest-deep-perl.

 -- gregor herrmann <gregoa@debian.org>  Fri, 07 Jul 2023 12:30:14 +0200

libhtml-mason-perl (1:1.59-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.5.1, no changes needed.
  * Apply multi-arch hints. + libhtml-mason-perl-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 15:14:41 +0100

libhtml-mason-perl (1:1.59-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Wrap long lines in changelog entries: 0.87-2.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.
  * Import upstream version 1.59.
  * Update debian/upstream/metadata.
  * Update years of upstream and packaging copyright.
  * Install new upstream docs.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.
  * Set upstream metadata fields: Bug-Submit.
  * Drop ancient Conflicts/Replaces.
  * Install upstream scripts as examples.
  * Mention module name in long description.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 May 2020 01:19:29 +0200

libhtml-mason-perl (1:1.58-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Drop git-debcherry framework as per decision of the pkg-perl BoF at
    DebConf17. Unapply patches, update patch headers, remove git notes,
    remove debian/README.source.

  [ Florian Schlichting ]
  * Import upstream version 1.58
  * Update upstream metadata
  * Update copyright years
  * Bump dh compat to level 9
  * Let libhtml-mason-perl-doc be Priority: optional as well
  * Declare compliance with Debian Policy 4.1.1
  * Drop obsolete manpage-has-bad-whatis-entry.patch

 -- Florian Schlichting <fsfs@debian.org>  Mon, 06 Nov 2017 23:22:32 +0100

libhtml-mason-perl (1:1.56-1) unstable; urgency=medium

  * Convert packaging to git-debcherry
  * Add debian/upstream/metadata
  * Import upstream version 1.56
  * Update copyright years
  * Let git-debcherry refresh patches, drop
    03_cgi_param_disable_list_context_warning.patch (applied upstream)
  * Drop README from docs
  * Add manpage-has-bad-whatis-entry.patch

 -- Florian Schlichting <fsfs@debian.org>  Tue, 18 Aug 2015 17:57:59 +0200

libhtml-mason-perl (1:1.54-2) unstable; urgency=medium

  * Explicitly (build) depend on libcgi-pm-perl.
    CGI.pm is leaving perl core. (Closes: #785037)
  * Update years of packaging copyright.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Mon, 11 May 2015 23:13:57 +0200

libhtml-mason-perl (1:1.54-1) unstable; urgency=medium

  [ Florian Schlichting ]
  * Import Upstream version 1.54

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Dominic Hargreaves ]
  * Fix warnings with newer CGI.pm by applying patch from
    Kevin Falcone (Closes: #765477)
  * Update Standards-Version (no changes)
  * Add myself to Uploaders

 -- Dominic Hargreaves <dom@earth.li>  Sun, 19 Oct 2014 18:17:04 +0100

libhtml-mason-perl (1:1.52-1) unstable; urgency=low

  * Imported Upstream version 1.52 (Closes: #574513)
  * Change upstream contact to Jonathan Swartz
  * Add myself to uploaders
  * Add libhtml-parser-perl also into build-dependencies

 -- Xavier Guimard <x.guimard@free.fr>  Sat, 12 Oct 2013 07:23:35 +0200

libhtml-mason-perl (1:1.51-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/control: drop speedy-cgi-perl from Suggests. The package is
    going away soon.

  [ Florian Schlichting ]
  * Imported Upstream version 1.51
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Bumped Standards-Version to 3.9.4 (no change)
  * Add Description: header to patches

 -- Florian Schlichting <fsfs@debian.org>  Sat, 25 May 2013 20:06:46 +0200

libhtml-mason-perl (1:1.50-1) unstable; urgency=low

  * Imported Upstream version 1.50.
  * Bumped Standards-Version to 3.9.3 (use copyright-format 1.0).
  * Bumped copyright years.
  * Dropped 03_spelling.patch, applied upstream.
  * Removed packaging infrastructure for htdocs, which are no longer shipped
    by upstream.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Tue, 14 Aug 2012 22:04:24 +0200

libhtml-mason-perl (1:1.48-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 1.48
    + Fix 'Unitialize value warnings when calling subcomponents' bug
      (Closes: #658260).

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 04 Feb 2012 15:44:12 +0100

libhtml-mason-perl (1:1.47-1) unstable; urgency=low

  * Imported Upstream version 1.47.
  * No longer install files to /var/www in postinst (Closes: #646209).
  * Merge libhtml-mason-perl-examples into libhtml-mason-perl-doc.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sun, 13 Nov 2011 02:08:11 +0000

libhtml-mason-perl (1:1.46-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Florian Schlichting ]
  * debian/mason_apache2_example.conf: Apply patch from BTS to work around an
    Apache2 bug preventing index.html to be shown when the URI specifies a
    directory (Closes: #451714).  Thanks, Andreas Krueger!
  * Update README.Debian so that the sample config provided actually works
    with current versions of Apache (Closes: #537692).
  * Imported Upstream version 1.46
  * Add myself to Uploaders.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Thu, 29 Sep 2011 22:06:58 +0000

libhtml-mason-perl (1:1.45-1) unstable; urgency=low

  [ Ivan Kohler ]
  * Take over for the Debian Perl Group
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza). Changed: Maintainer set to Debian Perl Group
    <pkg-perl-maintainers@lists.alioth.debian.org> (was: Ivan Kohler
    <ivan-debian@420.am>); Ivan Kohler <ivan-debian@420.am> moved to
    Uploaders.

  * New upstream release
    + Fixes warnings under perl 5.12 (closes: Bug#615593)

  [ gregor herrmann ]
  * Fix watch file.
  * Switch to source format 3.0 (quilt). Remove quilt framework.
  * Bump debhelper to >= 8. Drop cdbs.
  * Refresh patches.
  * Add Danish debconf translation; thanks to Joe Dalton (closes: #605327).
  * Convert debian/copyright to DEP5 format.
  * Set Standards-Version to 3.9.2; remove version from perl build dependency.
  * debian/control: update build and runtime dependencies (remove unneeded
    versions and packages that are in perl core, add ${misc:Depends},
    additional build dependencies and Suggests).
  * debian/rules: use /usr/share/cdbs/1/class/perl-makemaker.mk.
  * Use "set -e" in maintainer scripts.
  * Add /me to Uploaders.
  * Add a patch to fix some spelling mistakes.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 Jul 2011 14:26:12 +0200

libhtml-mason-perl (1:1.44-1) unstable; urgency=low

  * New upstream release
    + $m->flush_buffer is now ignored when inside $m->scomp or $m->content
      (closes: Bug#436507)

 -- Ivan Kohler <ivan-debian@420.am>  Mon, 22 Mar 2010 13:06:04 -0700

libhtml-mason-perl (1:1.42-1) unstable; urgency=low

  * New upstream release (closes: Bug#541191)
    + Fixes segfault when using alter_superclass (closes: Bug#491483)
    + Fixes documentation quoting in Component.pm (closes: Bug#462324)
  * Russian po-debconf translation (Closes: #538652)
  * New maintainer.  Huge thanks to Charles for his excellent work for
    many years.

 -- Ivan Kohler <ivan-debian@420.am>  Wed, 12 Aug 2009 14:38:35 -0700

libhtml-mason-perl (1:1.39-1) unstable; urgency=low

  * New upstream release
  * Don't copy examples into inexistent /usr/lib/cgi-bin (Closes: #435607)

 -- Charles Fry <cfry@debian.org>  Thu, 24 Jul 2008 13:22:58 -0400

libhtml-mason-perl (1:1.36-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Call realclean, to fix FTBFS if built twice in a row (Closes: #424512)
  * Added Homepage control field
  * Fixed doc-base section

 -- Peter Eisentraut <petere@debian.org>  Mon, 16 Jun 2008 13:59:34 +0200

libhtml-mason-perl (1:1.36-2) unstable; urgency=low

  * New dependency on libhtml-parser-perl (Closes: #434344)

 -- Charles Fry <cfry@debian.org>  Tue, 24 Jul 2007 09:03:37 -0400

libhtml-mason-perl (1:1.36-1) unstable; urgency=low

  * New upstream release (Closes: #425937)
  * Removed build dependency on libapache-request-perl (Closes: #432499)
  * Portuguese translation for debconf messages (Closes: #418930)

 -- Charles Fry <cfry@debian.org>  Thu, 12 Jul 2007 13:27:48 -0400

libhtml-mason-perl (1:1.35-3) unstable; urgency=low

  * Dutch po-debconf translation (Closes: #415516)
  * Conditional use of debconf in postrm (Closes: #416899)
  * Fixed encoding incompatibility between CGIHandler and ApacheHandler
    (Closes: #416606)

 -- Charles Fry <cfry@debian.org>  Sat, 31 Mar 2007 08:11:03 -0400

libhtml-mason-perl (1:1.35-2) unstable; urgency=low

  * German po-debconf template translation (Closes: #400702)
  * Spanish po-debconf translation (Closes: #403518)

 -- Charles Fry <cfry@debian.org>  Tue,  9 Jan 2007 13:03:27 -0500

libhtml-mason-perl (1:1.35-1) unstable; urgency=low

  * New upstream release

 -- Charles Fry <cfry@debian.org>  Sat, 11 Nov 2006 11:38:20 -0500

libhtml-mason-perl (1:1.33-2) unstable; urgency=low

  * Move clean target dependencies to Build-Depends
  * Update to standards version 3.7.2
  * New maintainer email address

 -- Charles Fry <cfry@debian.org>  Wed,  5 Jul 2006 13:12:28 -0400

libhtml-mason-perl (1:1.33-1) unstable; urgency=low

  * New upstream release
  * ApacheUserDirHandler supports Apache2, thanks to Troy Davis
    <tdavis@tdavis.org> and Derek W. Poon <derekp@ece.ubc.ca>
    (Closes: #345405)

 -- Charles Fry <debian@frogcircus.org>  Tue, 30 May 2006 10:44:03 -0400

libhtml-mason-perl (1:1.32-1) unstable; urgency=low

  * New upstream release

 -- Charles Fry <debian@frogcircus.org>  Tue,  3 Jan 2006 15:35:32 -0500

libhtml-mason-perl (1:1.31.01-2) unstable; urgency=low

  * Swedish debconf templates translation, thanks to Daniel Nylander
    <yeager@lidkoping.net> (Closes: #333135)

 -- Charles Fry <debian@frogcircus.org>  Mon, 10 Oct 2005 12:21:48 -0400

libhtml-mason-perl (1:1.31.01-1) unstable; urgency=low

  * New upstream release

 -- Charles Fry <debian@frogcircus.org>  Tue, 23 Aug 2005 16:24:49 -0400

libhtml-mason-perl (1:1.30-1) unstable; urgency=low

  * New upstream release
  * Initial Czech translation of debconf messages, thanks to
    Miroslav Kure <kurem@upcase.inf.upol.cz> (Closes: #316888)

 -- Charles Fry <debian@frogcircus.org>  Mon,  1 Aug 2005 02:18:35 -0400

libhtml-mason-perl (1:1.29.02-1) unstable; urgency=low

  * New upstream release
  * Build-depends on libmodule-build-perl, and accompanying new build
    parameters (thanks to Brendan O'Dea)
  * New mod_perl doesn't have Apache2.pm, removed from sample config

 -- Charles Fry <debian@frogcircus.org>  Fri, 24 Jun 2005 13:52:32 -0400

libhtml-mason-perl (1:1.28-2) unstable; urgency=low

  * Fixed 'Powered by Mason' image in example, thanks to Kim Hansen
    (Closes: #284592)
  * Rewrote README.Debian, integrating information on current best practices
    (Closes: #301225)
  * Changed file extensions in examples per current best practices
    (Closes: #301227)
  * Updated speedycgi-handler.cgi reference to point to examples package
    (Closes: #301224)
  * Removed dump-request example, as the new version of Mason breaks it,
    and a similar test is included in the docs package (Closes: #284615)
  * Moved custom scripts from examples to doc
  * Formatting changes in example configuration scripts

 -- Charles Fry <debian@frogcircus.org>  Fri, 17 Jun 2005 15:59:02 -0400

libhtml-mason-perl (1:1.28-1) unstable; urgency=low

  * New maintainer (Closes: #312836)
  * New upstream release (Closes: #290224, #286811)
  * Added Vietnamese debconf translation, thanks to Clytie Siddall
    (Closes: #312235)
  * Added Japanese debconf translation, thanks to Hideki Yamane
    (Closes: #258779)
  * Don't create spurious /etc/apache, thanks to Jeff Bailey (Closes: #269093)
  * Moved /var/lib/mason to /var/cache/mason for FHS compliance
    (Closes: #291652)

 -- Charles Fry <debian@frogcircus.org>  Wed, 15 Jun 2005 13:02:46 -0400

libhtml-mason-perl (1:1.26-1) unstable; urgency=low

  * New upstream release
  * apache-perl depends on libapache-mod-perl, so remove apache-perl from
    the Recommends field; no difference, just a simplification.
  * Debconf text updated, and translations correctly applied now. Thanks
    to Christian and Denis for their patience.

 -- Steve Haslam <araqnid@debian.org>  Wed,  7 Apr 2004 13:45:32 +0100

libhtml-mason-perl (1:1.25-6) unstable; urgency=low

  * Use invoke-rc.d instead of running apache*ctl directly (Closes: #241437)
  * Include French debconf translation (Closes: #238467)

 -- Steve Haslam <araqnid@debian.org>  Thu,  1 Apr 2004 18:54:55 +0100

libhtml-mason-perl (1:1.25-4) unstable; urgency=low

  * Ask libhtml-mason-perl/auto_restart_apache if the config fragment
    exists OR install_examples is true. So if the user says "no" to
    install_examples via dpkg-reconfigure to uninstall the examples, then
    they still get a chance to stop apache getting gracefulled again.
  * Use /var/lib/mason as the default data_dir setting as opposed to a
    server_root_relative path, provided /var/lib/mason exists. This aviods
    having to use a "mason" symlink in each of /etc/apache,
    /etc/apache-perl etc.
  * Correspondingly, remove the /etc/apache/mason symlink.
  * -examples: update postinst and config to look for apache-perl or
    apache-ssl being installed, and use their conf.d directories. This
    installs in *all* the apache1 variants found, if you have multiple
    variants installed, it could get excessive.
    (Closes: #236734)
  * -examples: Remove dependency on "apache", replace with recommendation
    of apache variants or apache2, and also recommend mod_perl (via
    apache-perl or a mod_perl DSO package)
  * -examples: Update postrm to remove our stuff from apache-perl etc. too
  * -examples: Also handle apache2, using a slightly different config file.
    This config file loads Apache::compat, which causes a performance hit.

 -- Steve Haslam <araqnid@debian.org>  Mon,  8 Mar 2004 12:38:03 +0000

libhtml-mason-perl (1:1.25-3) unstable; urgency=low

  * Separate out the manual into libhtml-mason-perl-doc and the examples
    into libhtml-mason-perl-examples. (Closes: #236230)
  * Remove the examples even if not being purged, otherwise we can leave
    the user with a broken mod_perl configuration. Also, with the examples
    being a separate package, this simply makes more sense.
  * dpkg-reconfigure libhtml-mason-perl-examples now installs/removes the
    examples, rather than having to remove the package. (Even if the
    examples are not "installed", they are still in
    /usr/share/doc/libhtml-mason-perl-examples/examples)
  * Restructured libhtml-mason-perl.README.Debian, which is now split
    between that file and
    libhtml-mason-perl-examples.README.Debian. Hopefully the documentation
    still makes sense.

 -- Steve Haslam <araqnid@debian.org>  Fri,  5 Mar 2004 13:40:41 +0000

libhtml-mason-perl (1:1.25-2) unstable; urgency=low

  * Move speedy-cgi-perl to Suggests
  * Looks like someone upstream rewrote the example httpd.conf (Closes: #231475)
  * Create /var/lib/mason as a directory owned by www-data.
  * Create /etc/apache/mason as a symlink to /var/lib/mason. So now the
    default MasonDataDir setting should work out-of-the-box.
  * Update my examples to include show-cookies (which can get cookies from
    CGI or Apache::Request), and to allow use of show-headers under pure
    CGI (which now works, due to use of FakeApache.pm)
  * Now automagically install examples for the user to peruse on the local
    Apache. Ask for permission first via debconf. (Closes: #68597)

 -- Steve Haslam <araqnid@debian.org>  Thu,  4 Mar 2004 18:25:46 +0000

libhtml-mason-perl (1:1.25-1) unstable; urgency=low

  * New upstream release (Closes: #229083)
  * Thanks for the patience from Marc Brockschmidt for:
    * Fixing typos in README (Closes: #197283)
    * Updating libexception-class-perl dependency (Closes: #202286, #213579)
  * Example httpd.conf missing ">" on line 22 (#231475, upstream)
  * FakeApache.pm is included in this upload (Closes: #231316)
  * Move debhelper Build-Depends to Build-Depends-Indep.

 -- Steve Haslam <araqnid@debian.org>  Mon,  9 Feb 2004 12:40:06 +0000

libhtml-mason-perl (1:1.21-1.2) unstable; urgency=low

  * Non-maintainer upload (yet another)
  * debian/control: Fix typo, now depend on the right version of
    libclass-exception-perl (1.10-1) (Closes: #202286)

 -- Marc Brockschmidt <marc@dch-faq.de>  Wed, 21 Jan 2004 18:53:52 +0100

libhtml-mason-perl (1:1.21-1.1) unstable; urgency=low

  * Non-maintainer upload
  * Rebuild against new packages
  * Fixed failing tests 9-10 in t/04-misc.t (Closes: #213579)
  * debian/control:
    - Updated dependencies. (Closes: #202286)
    - Bumped Standards-Version to 3.6.1
    - Moved debhelper to Build-Depends
    - Changed Section to perl
  * Fixed Typos in README.Debian (Closes: #197283)

 -- Marc Brockschmidt <marc@dch-faq.de>  Sun, 14 Dec 2003 14:57:51 +0100

libhtml-mason-perl (1:1.21-1) unstable; urgency=low

  * New upstream version (Closes: #186678)
  * In fact, move the libapache-request-erpl|libapache2-mod-perl2
    dependency into "Suggests", because really Mason doesn't *depend* on
    these Apache specifics. (Closes: #178597)

 -- Steve Haslam <araqnid@debian.org>  Sat,  7 Jun 2003 11:31:55 +0100

libhtml-mason-perl (1:1.17-2) unstable; urgency=low

  * Change dependency on libapache-request-perl to libapache-request-perl
    | libapache2-mod-perl2, which obvaites the need for
    libapache-request-perl (Closes: #196386)

 -- Steve Haslam <araqnid@debian.org>  Sat,  7 Jun 2003 10:43:29 +0100

libhtml-mason-perl (1:1.17-1) unstable; urgency=low

  * New upstream version
  * Add mason_example tree, updated README.Debian to use that as an
    example and also to show how to use SpeedyCGI.

 -- Steve Haslam <araqnid@debian.org>  Sun, 19 Jan 2003 03:26:16 +0000

libhtml-mason-perl (1:1.16-1) unstable; urgency=low

  * New upstream version (Closes: #176557)

 -- Steve Haslam <araqnid@debian.org>  Fri, 17 Jan 2003 12:41:40 +0000

libhtml-mason-perl (1:1.15-1) unstable; urgency=low

  * New upstream version

 -- Steve Haslam <araqnid@debian.org>  Tue, 15 Oct 2002 21:11:46 +0100

libhtml-mason-perl (1:1.14-1) unstable; urgency=low

  * New upstream version

 -- Steve Haslam <araqnid@debian.org>  Tue,  8 Oct 2002 10:30:53 +0100

libhtml-mason-perl (1:1.13-3) unstable; urgency=low

  * In HTML::Mason::ApacheUserDirHandler, customise in_package and
    apache_status_title for each user.
  * Added "use Apache::Log" in ApacheUserDirHandler

 -- Steve Haslam <araqnid@debian.org>  Fri,  4 Oct 2002 17:28:48 +0100

libhtml-mason-perl (1:1.13-2) unstable; urgency=low

  * Replace HTML::Mason::ApacheUserDirHandler, which got lost during
    updating to the new upstream version.

 -- Steve Haslam <araqnid@debian.org>  Mon, 16 Sep 2002 12:17:27 +0100

libhtml-mason-perl (1:1.13-1) unstable; urgency=low

  * debian/control: Fix Section: to match override file
  * New upstream version (Closes: #160746)

 -- Steve Haslam <araqnid@debian.org>  Fri, 13 Sep 2002 14:09:06 +0100

libhtml-mason-perl (1.1201-2) unstable; urgency=low

  * Wrote HTML::Mason::ApacheUserDirHandler package, and a README.Debian
    file giving some quick instructions on setting up Apache to work with
    HTML::Mason.

 -- Steve Haslam <araqnid@debian.org>  Mon,  9 Sep 2002 14:20:54 +0100

libhtml-mason-perl (1.1201-1) unstable; urgency=low

  * New upstream release

 -- Steve Haslam <araqnid@debian.org>  Mon, 12 Aug 2002 20:14:25 +0100

libhtml-mason-perl (1.05-1) unstable; urgency=low

  * Updated version while I sort out getting 1.201 installable

 -- Steve Haslam <araqnid@debian.org>  Fri,  2 Aug 2002 13:00:22 +0100

libhtml-mason-perl (1.04-1) unstable; urgency=low

  * New upstream release

 -- Steve Haslam <araqnid@debian.org>  Thu, 13 Dec 2001 19:32:54 +0000

libhtml-mason-perl (1.03-1) unstable; urgency=low

  * New upstream release

 -- Steve Haslam <araqnid@debian.org>  Mon,  4 Jun 2001 05:26:34 +0100

libhtml-mason-perl (1.02-1) unstable; urgency=low

  * New upstream release
  * New maintainer
  * Place Perl modules in /usr/share/perl5 for new Perl policy (closes #95508)

 -- Steve Haslam <araqnid@debian.org>  Fri, 18 May 2001 19:18:39 +0100

libhtml-mason-perl (0.896-1) unstable; urgency=low

  * New upstream release (closes: bug#83730)

 -- Michael Alan Dorman <mdorman@debian.org>  Sun, 28 Jan 2001 12:56:21 -0500

libhtml-mason-perl (0.89-1) unstable; urgency=low

  * New upstream release

 -- Michael Alan Dorman <mdorman@debian.org>  Fri, 15 Sep 2000 08:22:42 -0400

libhtml-mason-perl (0.88-1) unstable; urgency=low

  * New upstream release

 -- Michael Alan Dorman <mdorman@debian.org>  Thu, 31 Aug 2000 09:59:33 -0400

libhtml-mason-perl (0.87-2) unstable; urgency=low

  * Applied patch to correct whitespace issue (taken from message
    <0FW500MDRMK6NP@mta6.snfc21.pbi.net>)

 -- Michael Alan Dorman <mdorman@debian.org>  Fri, 16 Jun 2000 13:37:35 -0400

libhtml-mason-perl (0.87-1) unstable; urgency=low

  * New upstream release

 -- Michael Alan Dorman <mdorman@debian.org>  Wed, 31 May 2000 20:30:09 -0400

libhtml-mason-perl (0.86-1) unstable; urgency=low

  * New upstream version, reverse numerous patches.

 -- Michael Alan Dorman <mdorman@debian.org>  Fri, 19 May 2000 11:05:31 -0400

libhtml-mason-perl (0.85-3) unstable; urgency=low

  * Applied new parser patch, and another couple of small patches.

 -- Michael Alan Dorman <mdorman@debian.org>  Wed, 10 May 2000 15:25:12 -0400

libhtml-mason-perl (0.85-2) unstable; urgency=low

  * Applied patch for bug in new Parser code.

 -- Michael Alan Dorman <mdorman@debian.org>  Tue,  9 May 2000 12:38:05 -0400

libhtml-mason-perl (0.85-1) unstable; urgency=low

  * New upstream version, incorporates patches from last version.

 -- Michael Alan Dorman <mdorman@debian.org>  Mon,  8 May 2000 20:41:29 -0400

libhtml-mason-perl (0.81-2) unstable; urgency=low

  * Apply a patch for DeclineDirectory = 0.

 -- Michael Alan Dorman <mdorman@debian.org>  Sat, 18 Mar 2000 16:03:15 -0500

libhtml-mason-perl (0.81-1) unstable; urgency=low

  * New upstream version.

 -- Michael Alan Dorman <mdorman@debian.org>  Tue, 22 Feb 2000 13:05:42 -0500

libhtml-mason-perl (0.80-2) unstable; urgency=low

  * Applied a couple of minor patches from the mailing list.

 -- Michael Alan Dorman <mdorman@debian.org>  Thu, 27 Jan 2000 15:19:53 -0500

libhtml-mason-perl (0.80-1) unstable; urgency=low

  * New upstream version.

 -- Michael Alan Dorman <mdorman@debian.org>  Mon, 24 Jan 2000 09:38:41 -0500

libhtml-mason-perl (0.72-1) unstable; urgency=low

  * New upstream version.

 -- Michael Alan Dorman <mdorman@debian.org>  Tue, 19 Oct 1999 11:35:02 -0400

libhtml-mason-perl (0.71-1) unstable; urgency=low

  * New upstream version.
  * New maintainer.  Thanks to Craig for getting this out there.

 -- Michael Alan Dorman <mdorman@debian.org>  Tue, 14 Sep 1999 18:59:37 -0400

libhtml-mason-perl (0.7-2) unstable; urgency=low

  * Fix for mc_suppress_http_header.
  * Fix for top_level_predicate.

 -- Michael Alan Dorman <mdorman@debian.org>  Fri,  3 Sep 1999 11:04:35 -0400

libhtml-mason-perl (0.7-1) unstable; urgency=low

  * New upstream release.  Personal NMU.

 -- Michael Alan Dorman <mdorman@debian.org>  Thu,  2 Sep 1999 13:31:35 -0400

libhtml-mason-perl (0.6.1-1) unstable; urgency=low

  * New upstream release.  Personal NMU.

 -- Michael Alan Dorman <mdorman@debian.org>  Thu, 29 Jul 1999 11:29:12 -0400

libhtml-mason-perl (0.6-1) unstable; urgency=low

  * New Upstream release
  * changed to use Storable by default rather than Data::Dumper
  * added Depends: libstorable-perl
  * moved Config.pm to /etc/Mason-Config.pm and symlinked
  * changed Depends: libapache-mod-perl to Suggests:
  * added Suggests: speedy-cgi-perl

 -- Craig Sanders <cas@taz.net.au>  Thu, 22 Jul 1999 09:06:15 +1000

libhtml-mason-perl (0.51-1) unstable; urgency=low

  * Initial Release

 -- Craig Sanders <cas@taz.net.au>  Mon,  5 Jul 1999 10:44:00 +1000
